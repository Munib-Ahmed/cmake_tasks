#include <iostream>
#include "nlohmann/json.hpp"
//#include "json/single_include/nlohmann/json.hpp"
using json = nlohmann::json;
using namespace std;

int main()
{
    json j = {{"pi", 3.141}, {"happy", true}};
    cout<< j<<endl;
    return 0;
}
