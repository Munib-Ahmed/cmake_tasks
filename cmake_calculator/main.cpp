#include <iostream>
#include "calculator.h"
using namespace std;

int main()
{
    Welcome();
    double number_1 = 0, number_2 = 0;
    
    cout<<"Enter the first number = ";
    cin>>number_1;
    cout<<"Enter the second number = ";
    cin>>number_2;

    add(number_1, number_2);
    subtract(number_1, number_2);
    multiply(number_1, number_2);
    divide(number_1, number_2);
    
    return 0;
}
