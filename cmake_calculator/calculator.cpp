#include <iostream>
#include "calculator.h"
using namespace std;

void Welcome()
{
    cout << "Welcome to the Calculator" << endl << endl;
}
void add(double num_1, double num_2)
{
    cout << "Addition = " << (num_1+num_2) << endl;
}
void subtract(double num_1, double num_2)
{
    cout << "Subtraction = " << (num_1-num_2) << endl;
}
void multiply(double num_1, double num_2)
{
    cout << "Multiplication = " << (num_1*num_2) << endl;
}
void divide(double num_1, double num_2)
{
    cout << "Division = " << (num_1/num_2) << endl;
}
