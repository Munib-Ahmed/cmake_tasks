#include <iostream>
#include <map>
#include <string>
#include <iterator>
using namespace std;

class student 
{
	private:
  		struct student_record 
		{
    		std::string roll_no;
    		int age;
    		float cgpa;
  		};
		student_record st_object;

  		std::map<std::string /*subject name*/, int /*marks*/> result;

	public:
  		int get_subject_marks(std::string subject)
		{
			cout<<"find function"<<endl;
			if (result.find(subject) == result.end())
			{
				cout<<"course not found"<<endl;
				return -1;
			}
			else
			{
				std::map<std::string, int>::iterator itr = result.find(subject);
				return itr->second;
				//cout<<itr->second;
				//return result.find(subject);
			}
		}
  		void set_subject_marks(std::string subject, int marks)
		{
			result.insert(pair<std::string , int>(subject, marks));
		}
  		void print_all_marks()
		{
			cout << '\t' << "Subjects "<< '\t' << " Marks" << '\n';
			std::map<std::string, int>::iterator itr;
			for (itr = result.begin(); itr != result.end(); ++itr)
			{
        			cout << '\t' <<itr->first << '\t'<< '\t' <<itr->second << '\n';
    			}
		}
	  	void set_cgpa(float gpa)
		{
			st_object.cgpa = gpa;
		}
	  	void set_age(int age)
		{
			st_object.age = age;
		}
	  	void set_roll_no(string roll_no)
		{
			st_object.roll_no = roll_no;
		}
	  	float get_cgpa()
		{
			return st_object.cgpa;
		}
	  	int get_age()
		{
			return st_object.age;
		}
	  	string get_roll_no()
		{
			return st_object.roll_no;
		}
};
