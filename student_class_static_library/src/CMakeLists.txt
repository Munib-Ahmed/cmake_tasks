add_library(
student_library STATIC
student.cpp
)

target_include_directories(student_library PUBLIC ${PROJECT_SOURCE_DIR}/include)
