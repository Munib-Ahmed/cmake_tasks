#include <map>
#include <string>
#include <iterator>
using namespace std;

class student 
{
	private:
  		struct student_record 
		{
    			std::string roll_no;
    			int age;
    			float cgpa;
  		};
		student_record st_object;

  		std::map<std::string /*subject name*/, int /*marks*/> result;

	public:
		student();
  		int get_subject_marks(std::string subject);
  		void set_subject_marks(std::string subject, int marks);
  		void print_all_marks();
	  	void set_cgpa(float gpa);
	  	void set_age(int age);
	  	void set_roll_no(string roll_no);
	  	float get_cgpa();
	  	int get_age();
	  	string get_roll_no();
	  	~student();
};
