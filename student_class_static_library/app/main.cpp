#include <iostream>
#include "student.h"
#include <string>
using namespace std;

int main()
{
	cout << "Hi, Everyone" << endl;
	student s;

	s.set_subject_marks("english", 90);
	s.set_subject_marks("maths", 90);
	s.set_subject_marks("urdu", 90);
	s.set_subject_marks("science", 100);
	s.print_all_marks();
	cout<<s.get_subject_marks("urdu")<<endl;
	s.set_cgpa(5.2);	
	s.set_roll_no("i15-0811");
	s.set_age(22);
	cout << "Your CGPA = " << s.get_cgpa()<<endl;
	cout << "Your Roll no. = " << s.get_roll_no()<<endl;
	cout << "Your age = " << s.get_age()<<endl;
	return 0;
}

//cmake -DCMAKE_BUILD_TYPE=Release ..

